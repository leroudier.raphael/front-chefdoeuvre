import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileBlocComponent } from './profile-bloc.component';

describe('ProfileBlocComponent', () => {
  let component: ProfileBlocComponent;
  let fixture: ComponentFixture<ProfileBlocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileBlocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileBlocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
