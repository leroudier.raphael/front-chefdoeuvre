import { Component, OnInit } from '@angular/core';
import { Client } from '../entity/client';
import { AuthService } from '../repository/auth.service';
import { MatDialog } from "@angular/material/dialog";
import { UpdateProfilComponent } from '../update-profil/update-profil.component';

@Component({
  selector: 'app-profile-bloc',
  templateUrl: './profile-bloc.component.html',
  styleUrls: ['./profile-bloc.component.css']
})
export class ProfileBlocComponent implements OnInit {
  user:Client;
hide:Boolean;
hide2:Boolean;
  constructor(private authService:AuthService, public dialog: MatDialog) { }

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.user = user
    });  }
  openUserForm(){
    const dialogRef = this.dialog.open(UpdateProfilComponent, {
      height: "600px",
      width: "300px",
      data:{user:this.user}
    })
    dialogRef.afterClosed().subscribe()
  }
 delete(){
   
 }
}
