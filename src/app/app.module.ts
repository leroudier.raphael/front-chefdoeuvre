import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProfileBlocComponent } from './profile-bloc/profile-bloc.component';
import { WeatherBlocComponent } from './weather-bloc/weather-bloc.component';
import { ToDoBlocComponent } from './to-do-bloc/to-do-bloc.component';
import { HomeComponent } from './home/home.component';
import { ProductListComponent } from './product-list/product-list.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { JwtService } from './interceptor/jwt.service';
import { AccountComponent } from './account/account.component';
import { GardensListComponent } from './gardens-list/gardens-list.component';
import { GardenFormComponent } from './garden-form/garden-form.component';
import { UpdateProfilComponent } from './update-profil/update-profil.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MatSliderModule } from '@angular/material/slider';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import {MatIconModule} from '@angular/material/icon';
import {MatGridListModule} from '@angular/material/grid-list';
import { NavbarComponent } from './navbar/navbar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import { TodoFormComponent } from './todo-form/todo-form.component';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatDialogModule} from '@angular/material/dialog';
import { MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material/dialog'
import {MatExpansionModule} from '@angular/material/expansion';
import {MatBadgeModule} from '@angular/material/badge';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from "@angular/material";


@NgModule({
  declarations: [
    AppComponent,
    ProfileBlocComponent,
    WeatherBlocComponent,
    ToDoBlocComponent,
    HomeComponent,
    ProductListComponent,
    LoginComponent,
    RegisterComponent,
    AccountComponent,
    GardensListComponent,
    GardenFormComponent,
    
    UpdateProfilComponent,
    NavbarComponent,
    TodoFormComponent,    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatSliderModule,
    BrowserAnimationsModule,
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    MatCardModule,
    MatButtonModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatInputModule,
    MatRadioModule,
    MatIconModule,
    MatGridListModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatMenuModule,
    MatBottomSheetModule,
    MatExpansionModule,
    MatDialogModule,
    MatBadgeModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    
  ], entryComponents: [
    TodoFormComponent, GardenFormComponent, UpdateProfilComponent
  ],
  providers: [ {provide: HTTP_INTERCEPTORS, useClass: JwtService, multi: true},{provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}],
  bootstrap: [AppComponent]
})
export class AppModule { }
