import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService } from './repository/auth.service';
import { map, tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService:AuthService, private router:Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      /**
       * Une guard peut également renvoyé un observable/promise de booléen,
       * dans le cas où la vérification doit faire appel à de l'asynchrone
       * Ici on déclenche notre méthode getUser() qui fait un appel ajax
       * pour récupérer le user côté back et selon si on récupère bien
       * un user ou non, on donne accès à la route ou pas
       */
    return this.authService.getUser().pipe(
      //On utilise le map pour transformer le Observable<User> en Observable<boolean>
      //En gros, s'il y a bien un user, l'observable contiendra maintenant true, sinon il contiendra false
      map(user => user !== null),
      /**
       * On utilise le catchError pour faire en sorte de renvoyer un Observable<false>
       * dans le cas où la requête ajax renverrait une erreur (401, ou 500)
       */
      catchError(() => of(false)),
      /**
       * On utilise le tap pour déclencher la redirection si jamais l'observable
       * contient un false
       */
      tap(access => {
        if(!access) {
          this.router.navigate(['']);
        }
      })
    );
  }
  
}
