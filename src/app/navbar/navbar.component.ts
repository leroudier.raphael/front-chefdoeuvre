import {Component,OnInit, NgModule} from '@angular/core';
import { AuthService } from '../repository/auth.service';
import { Router } from '@angular/router';
import { Client } from '../entity/client';
import { AdviceService } from '../repository/advice.service';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit{
  currentUser:Client;
    constructor(private repoAuth:AuthService,private repoAdvice:AdviceService, private router:Router) { }
hintAdvice:number;
  ngOnInit() {
    // recuperation du token pour un potientiel user
    this.repoAuth.user.subscribe(user => {
      this.currentUser = user
    });
    
  }
  logout() {
    this.repoAuth.logout();
    this.router.navigate(['']);
  }
}
