import { Component, OnInit, Input, Inject } from "@angular/core";
import { Garden } from "../entity/garden";
import { GardenService } from "../repository/garden.service";
import { ProductService } from "../repository/product.service";
import { Product } from "../entity/product";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { CategoryService } from "../repository/category.service";
import { Category } from "../entity/category";
import { AutoCompleteService } from "../repository/auto-complete.service";

@Component({
  selector: "app-garden-form",
  templateUrl: "./garden-form.component.html",
  styleUrls: ["./garden-form.component.css"]
})
export class GardenFormComponent implements OnInit {
  categoriesList;
  //garden declarer pour le form
  newGarden: Garden = {
    name: null,
    location: null,
    products: null
  };
  number;
  // Variable pour l'autocompletion de la localité
  addresses;

  @Input()
  selectedGarden: Garden;

  categories: Category[];
  productsList: Product[];

  constructor(
    private autoComplete: AutoCompleteService,
    private repo: ProductService,
    private repoCategory: CategoryService,
    private repoGarden: GardenService,
    public dialogRef?: MatDialogRef<GardenFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data?: any
  ) {
    if (data) {
      this.newGarden = data.garden;
      this.number = data.number;
    }
  }

  ngOnInit() {
    this.repo.findAll().subscribe(products => {
      (this.productsList = products), console.log(this.productsList);
    });
    this.repoCategory
      .findAll()
      .subscribe(categories => (this.categories = categories));
  }

  // Fonction pour creer un nuveau jardin et la faire persister

  // Fonction pour fermer la fenetre u formulaire
  onNoClick(): void {
    this.dialogRef.close();
   document.location.reload(true)
  }
  //  Fonction d' autocompletion de la ville avec une requete Ajax sur l'api Gouv
  search() {
    if (this.newGarden.location) {
      this.autoComplete.getAddresses(this.newGarden.location).subscribe(
        response => {
          (this.addresses = response), console.log(this.addresses);
        },
        error => console.log(error)
      );
    } else {
      this.addresses = [];
    }
  }
  // methode de validation du form qui switch en fonction de si l'id est null
  toValidate() {
    if (this.newGarden.id) {
      this.editGarden();
    } else {
      this.addGarden();
    }
  }
  addGarden() {
    this.repoGarden
      .add(this.newGarden)
      .subscribe(() => document.location.reload(true));
  }
  editGarden() {
    this.repoGarden
      .edit(this.newGarden)
      .subscribe(() => document.location.reload(true));
  }
}
