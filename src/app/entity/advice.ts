import {Product} from "../entity/product";
import { Subcategory } from './subcategory';
import { Category } from '../entity/category';
export interface Advice {
    id?:number;
    title:string;
    content: string;
    eventStart: any;
    eventEnd: any;
    categories: Category[];
    subcategories: Subcategory[];
    product: Product;  
}
