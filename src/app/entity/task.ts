import { ToDoList } from "./todolist";

export interface Task {
    id?:number;
    title:string;
    content: string;
    eventStart: any;
    eventEnd: any;
    toDoList?: ToDoList;
}
