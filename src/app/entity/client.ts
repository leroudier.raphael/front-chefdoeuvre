import { Garden } from "./garden";
import { ToDoList } from "./todolist";

export interface Client {
    id?:number;
    email:string;
    password: string;
    birthdate?:any;
    garden?: Garden[];
    toDoList?:ToDoList;
    
}
