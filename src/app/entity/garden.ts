import {Product} from "../entity/product";

export interface Garden {
    id?:number;
    name:string;
    location:string;
    creationDate?:any;
    client?: any;
    products?: Product[];
}
