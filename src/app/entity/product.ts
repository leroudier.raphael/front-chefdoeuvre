import {Category} from "../entity/category";
import {Advice} from "../entity/advice";
import { Subcategory } from './subcategory';
import {Garden} from "../entity/garden";

export interface Product {
    id?:number;
    name:string;
    path:string;
    imgPath:string;
    gardens: Garden[];
    category: Category;
    subcategory: Subcategory;
    advices: Advice[];
   
}
