import {Product} from "../entity/product";
import {Consumable} from "../entity/consumable";
import { Subcategory } from './subcategory';
export interface Category {
    id?:number;
    name:string;
    consumables: Consumable[];
    subcategories: Subcategory[];
    advices:any;
    products: Product[];  
}
