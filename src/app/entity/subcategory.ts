import {Product} from "../entity/product";
import { Category } from "./category";
import { Advice } from './advice';
import { Consumable } from './consumable';

export interface Subcategory {
    id?:number;
    name:string;
    category:Category;
    consumables:Consumable[];
    products:Product[];
    advices: Advice;
}
