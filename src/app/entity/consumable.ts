import { Category } from "../entity/category";
import { Subcategory } from "../entity/subcategory";
export interface Consumable {
    id?:number;
    name:string;
    path: string;
    imgPath: string;
    categories: Category[];
    subcategories: Subcategory[];
}
