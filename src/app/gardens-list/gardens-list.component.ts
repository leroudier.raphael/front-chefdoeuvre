import { Component, OnInit } from "@angular/core";
import { Garden } from "../entity/garden";
import { GardenService } from "../repository/garden.service";
import { AuthService } from "../repository/auth.service";
import { Client } from "../entity/client";
import { Product } from "../entity/product";
import { MatDialog } from "@angular/material/dialog";
import { GardenFormComponent } from "../garden-form/garden-form.component";
import * as moment from "moment";

@Component({
  selector: "app-gardens-list",
  templateUrl: "./gardens-list.component.html",
  styleUrls: ["./gardens-list.component.css"]
})
export class GardensListComponent implements OnInit {
  // variable pour overture/fermeture composant panel de angular material
  panelOpenState = false;
  currentMoment = moment().format("MMMM YYYY");
  gardens: Garden[];
oneGarden:Garden;

  user: Client;
  products: Product[];
 // variable pour switcher lenregistrement ou la modification de la fonction dans le component gardenFormComponent

  constructor(
    private repoGarden: GardenService,
    private authService: AuthService,
    public dialog: MatDialog,
    public dialog2: MatDialog
  ) {}

  ngOnInit() {
    this.authService.user.subscribe(user => (this.gardens = user.garden));
    
  }
  // fonction d'ouverture et fermeture du composant pour le formulaire d'ajout de Garden
  openForm() {
  
    const dialogRef = this.dialog.open(GardenFormComponent, {
      height: "600px",
      width: "300px",
      data:null
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
            
          });
          console.log(this.oneGarden);

  }
    // fonction d'ouverture et fermeture du composant pour le formulaire d'ajout de Garden
// ouvre le meme composant que la methode openForm() sauf pour linjection des données
  openUpdateForm() {
  
    const dialogRef2 = this.dialog2.open(GardenFormComponent, {
      height: "600px",
      width: "300px",
      data:{ garden: this.oneGarden, number:this.oneGarden.id}
    });
    dialogRef2.afterClosed().subscribe(result => {
      console.log(result);       
          });


  }
 // methode pour supprimer un Garden
  deleteGarden() {
    this.repoGarden
      .delete(this.oneGarden)
      .subscribe(() => document.location.reload(true));
  }
  
}
