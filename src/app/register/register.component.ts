import { Component, OnInit } from '@angular/core';
import { AuthService } from '../repository/auth.service';
import { Client } from '../entity/client';
import { Router } from '@angular/router';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

/** @title Input with a custom ErrorStateMatcher */

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  matcher = new MyErrorStateMatcher();
  
  user: Client = {
    email: null,
    password: null

  };
  repeat: string = null;
  message:string;
 // variable des components du form angular material
  hide = true;
  hide2 = true;


  constructor(private authRepo: AuthService, private router:Router) { }

  ngOnInit() {
  }

  register() {
   
    if (this.validForm()) {
      
      this.authRepo.addUser(this.user).subscribe(  () => this.router.navigate(['account']),
      data => this.message = data.error.message);
    
    }else {
      this.message = 
        Validators.email +"&"+ Validators.required
      
    }
  }

  validForm():boolean {
    return this.user.email && this.user.password && this.user.password === this.repeat;
    
  }

}
