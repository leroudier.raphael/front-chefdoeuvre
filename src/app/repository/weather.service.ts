import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class WeatherService {
url = "http://api.openweathermap.org/data/2.5/weather?q=";

  constructor(private http: HttpClient) { }
  getWeather(city){
    return this.http.get(this.url + city + "&appid=50dc5e0bc67b94bb26b4fde3c262800f&lang=fr&units=metric");
  }
}
