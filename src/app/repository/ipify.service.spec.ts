import { TestBed } from '@angular/core/testing';

import { IpifyService } from './ipify.service';

describe('IpifyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IpifyService = TestBed.get(IpifyService);
    expect(service).toBeTruthy();
  });
});
