import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class IpifyService {
private url = "https://api.ipify.org?format=json";
  constructor(private http: HttpClient) { }
  get(){
    return this.http.get<any>(this.url);
  }
}
