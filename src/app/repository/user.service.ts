import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../entity/client';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = 'http://localhost:8000/api/user';
  
  constructor(private http:HttpClient) { }
  add(user:Client): Observable<Client> {
    return this.http.post<Client>(this.url, user);
  }
  delete(user:Client): Observable<Client> {
    return this.http.delete<Client>(this.url + '/'+ user.id);
  }
  updateuser(user:Client) {
    this.http.patch<Client>(this.url + "/" + user.id, user)
  }
}
