import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Client } from '../entity/client';
import { tap, switchMap } from "rxjs/operators";
import { Observable, BehaviorSubject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url = 'http://localhost:8000/api/client';
  public user:BehaviorSubject<Client> = new BehaviorSubject(null);

  constructor(private http:HttpClient) { }

  addUser(user:Client) {
    return this.http.post<Client>(this.url+'/register', user);
  }
  updateUser(user:Client) {
console.log(user)
    return this.http.patch<Client>(this.url + "/edit/" + user.id, user)

  }
  login(username:string, password:string) {
    return this.http.post<{token:string}>('http://localhost:8000/api/login_check', {
      username,
      password
    }).pipe(
      tap(data => localStorage.setItem('token', data.token)),
      switchMap(() => this.getUser())
    );
  }

  getToken():string {
    return localStorage.getItem('token');
  }

  logout() {
    localStorage.removeItem('token');
    this.user.next(null);
  }

  getUser(): Observable<Client> {
    return this.http.get<Client>(this.url).pipe(
      tap(user => this.user.next(user))
    );
  }
}
