import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from '../entity/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private url = 'http://localhost:8000/api/task';
 
  constructor(private http:HttpClient) { }
  
  add(task:Task): Observable<Task> {
    return this.http.post<Task>(this.url, task);
  }
  delete(task:Task): Observable<Task> {
    return this.http.delete<Task>(this.url + '/delete/'+ task.id);
  }
}
