import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Advice } from '../entity/advice';
@Injectable({
  providedIn: 'root'
})
export class AdviceService {
  private url = 'http://localhost:8000/api/advice';
 
  constructor(private http:HttpClient) { }
  
  findAll(): Observable<Advice[]> {
    return this.http.get<Advice[]>(this.url);
  }
}
