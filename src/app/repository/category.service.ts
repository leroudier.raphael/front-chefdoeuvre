import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Category } from '../entity/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private url = 'http://localhost:8000/api/category';
 
  constructor(private http:HttpClient) { }
  
  findAll(): Observable<Category[]> {
    return this.http.get<Category[]>(this.url);
  }
}