import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
@Injectable({
  providedIn: "root"
})
export class CityService {
  private url =  "http://api.ipstack.com/" ;
  constructor(private http: HttpClient) {}
  get(ip): Observable<any> {
    return this.http.get<any>(this.url +
      ip +
      "?access_key=e94ca0a8c52e0ae7b9e3fb8f23ce0982");
  }
}
