import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../entity/product';



@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private url2 = 'http://localhost:8000/api/product';
  
  constructor(private http2:HttpClient) { }
  
  findAll(): Observable<Product[]> {
    return this.http2.get<Product[]>(this.url2);
  }

}
