import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Garden } from '../entity/garden';


@Injectable({
  providedIn: 'root'
})
export class GardenService {
  private url = 'http://localhost:8000/api/garden';
 
  constructor(private http:HttpClient) { }
  
  findAll(): Observable<Garden[]> {
    return this.http.get<Garden[]>(this.url);
  }

  add(garden:Garden): Observable<Garden> {
    return this.http.post<Garden>(this.url, garden);
  }

  delete(garden:Garden): Observable<Garden> {
    return this.http.delete<Garden>(this.url + '/delete/'+ garden.id);
  }
  
  edit(garden:Garden) { 
   return this.http.patch<Garden>(this.url + "/edit/" + garden.id, garden)
  }
}
