import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Commune } from '../entity/commune';


@Injectable({
  providedIn: 'root'
})
export class SearchAddressService  {

  constructor(private http:HttpClient) { }
 
  find():Observable<string[]> {
    return this.http.get<any>('https://geo.api.gouv.fr/communes/')
    .pipe(
      map(data => data.map(data => data))
    );
  }
}
