import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AutoCompleteService {

  constructor(private http:HttpClient) { }

  getAddresses(search:string):Observable<string[]> {
    return this.http.get<any>('https://api-adresse.data.gouv.fr/search/?q='+search+'&type=municipality&limit=10')
    .pipe(
      map(data => 
        data.features.map(feature => feature.properties.city)
        )
    );
  }
}
