import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductListComponent } from './product-list/product-list.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './auth.guard';
import { AccountComponent } from './account/account.component';
import { GardensListComponent } from './gardens-list/gardens-list.component';
import { ToDoBlocComponent } from './to-do-bloc/to-do-bloc.component';


const routes: Routes = [
{path:"list", component: ProductListComponent, canActivate: [AuthGuard]},
{path:"", component: HomeComponent},
  {path:"account", component: AccountComponent, canActivate: [AuthGuard]},
  {path:"todolist", component: ToDoBlocComponent, canActivate: [AuthGuard]},
  {path:"gardens", component: GardensListComponent, canActivate: [AuthGuard]},
  {path:"register", component: RegisterComponent},
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
