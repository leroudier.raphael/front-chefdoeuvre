import { Component, OnInit } from '@angular/core';
import { AuthService } from '../repository/auth.service';
import { Client } from '../entity/client';
import { Router } from '@angular/router';
import {FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  cheminImage  = "../../../img/logo.png";
  loginForm = new FormGroup({
  username:new FormControl('', [Validators.required, Validators.email]),
  password:new FormControl('')
});
// message de confirmation d'autentification
  message:string;
  // user actuelement connecté?
  currentUser:Client;

  // variable des components du form angular material
  hide = true;
 
  // function d'affichage d'erreur des champs de saisie
  getErrorMessage() {
    return this.loginForm.value.username.hasError('required') ? 'You must enter a value' :
    this.loginForm.value.username.hasError('email') ? 'Not a valid email' :
            '';
  }

  constructor(private repoAuth:AuthService, private router:Router) { }

  ngOnInit() {
    // recuperation du token pour un potientiel user
    this.repoAuth.user.subscribe(user => {
      this.currentUser = user
    });
  }

  // function de connection et de redirection
  login() {
    this.repoAuth.login(this.loginForm.value.username, this.loginForm.value.password).subscribe(
      () => this.router.navigate(['account']),
      data => this.message = 'Authentication Error.'
   )
   
  }
// function de deconnexion et de redirection
  logout() {
    this.repoAuth.logout();
    this.router.navigate(['']);
    this.message = '';
  }
}
