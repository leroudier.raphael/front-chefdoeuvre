import { Component, OnInit } from '@angular/core';
import {ProductService} from '../repository/product.service';
import {Product} from '../entity/product';
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products:Product[];
  constructor(private repo:ProductService) {}

  ngOnInit() {
    this.repo.findAll().subscribe( data => this.products = data);
    console.log(this.products)
  }

}
