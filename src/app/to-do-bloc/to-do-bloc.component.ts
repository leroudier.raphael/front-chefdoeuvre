import { Component, OnInit } from "@angular/core";
import { Task } from "../entity/task";
import { Client } from "../entity/client";
import { AuthService } from "../repository/auth.service";
import { TaskService } from "../repository/task.service";
import { FormControl, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { TodoFormComponent } from "../todo-form/todo-form.component";
import { Router } from '@angular/router';

@Component({
  selector: "app-to-do-bloc",
  templateUrl: "./to-do-bloc.component.html",
  styleUrls: ["./to-do-bloc.component.css"]
})
export class ToDoBlocComponent implements OnInit {
  currentDate = new Date();

  toDoList: Task[];

  selectedTask: Task;
  
  constructor(
    private authService: AuthService,
    private repoTask: TaskService,
    public dialog3: MatDialog,private router:Router
  ) {}

  ngOnInit() {
    this.authService.user.subscribe(user => {
    (this.toDoList = user.toDoList.tasks);
    });
  }
  openDialog() {
    const dialogRef3 = this.dialog3.open(TodoFormComponent, {
      height: "600px",
      width: "300px"
    });
    dialogRef3.afterClosed().subscribe();
    
  }
  deleteTask(task) {
    this.repoTask.delete(task).subscribe(() => document.location.reload(true)) ;  
  }
}
