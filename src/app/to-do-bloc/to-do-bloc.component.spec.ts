import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToDoBlocComponent } from './to-do-bloc.component';

describe('ToDoBlocComponent', () => {
  let component: ToDoBlocComponent;
  let fixture: ComponentFixture<ToDoBlocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToDoBlocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToDoBlocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
