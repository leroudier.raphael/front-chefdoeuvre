import { Component, Input, OnInit, Inject } from '@angular/core';
import { Task } from '../entity/task';
import { ToDoList } from '../entity/todolist';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { TaskService } from '../repository/task.service';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent implements OnInit {
  newTask:Task = {
    title:null,
    content: null,
    eventStart: null,
    eventEnd: null
  }
 
  @Input()
  toDoList:ToDoList;
  constructor(private repoTask:TaskService, public dialogRef3: MatDialogRef<TodoFormComponent>,
      ) {}

  onNoClick(): void {
    this.dialogRef3.close();
  }

  ngOnInit() {
  }

  addTask() {
    this.repoTask.add(this.newTask).subscribe(()=>document.location.reload());
 
  }
}
