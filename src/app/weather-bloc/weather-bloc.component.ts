import { WeatherService } from "../repository/weather.service";
import { Component, OnInit, Input, Output } from "@angular/core";
import { Garden } from "../entity/garden";

@Component({
  selector: "app-weather-bloc",
  templateUrl: "./weather-bloc.component.html",
  styleUrls: ["./weather-icons.css"]
})
export class WeatherBlocComponent implements OnInit {
  @Input()
  city;
  @Input()
  garden: Garden;
  @Output()
  dataWeather: any;
  weather;
  icon;

  constructor(private repoWeather: WeatherService) {}
  ngOnInit() {
    if (this.city) {
      this.repoWeather.getWeather(this.city).subscribe(
        data=> {this.weather = data, this. drawWeatherIcon()});

    }
  }
  
  drawWeatherIcon(){
    const weatherIcons = {
      Rain: "wi wi-day-rain",
      Clouds: "wi wi-day-cloudy",
      Clear: "wi wi-day-sunny",
      Snow: "wi wi-day-snow",
      Mist: "wi wi-day-fog",
      Drizzle: "wi wi-day-sleet"
    };
    const condition = this.weather.weather[0].main;
   this.icon= weatherIcons[
      condition
    ];
  }
}
