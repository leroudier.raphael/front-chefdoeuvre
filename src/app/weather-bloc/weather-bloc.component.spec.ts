import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherBlocComponent } from './weather-bloc.component';

describe('WeatherBlocComponent', () => {
  let component: WeatherBlocComponent;
  let fixture: ComponentFixture<WeatherBlocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherBlocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherBlocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
