import { Component, OnInit } from '@angular/core';
import { AuthService } from '../repository/auth.service';
import { Client } from '../entity/client';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  currentUser:Client;

  constructor(private repoAuth:AuthService) { }

  ngOnInit() {
    this.repoAuth.user.subscribe(user => this.currentUser = user);

  }

}
