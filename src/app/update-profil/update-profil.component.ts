import { Component, OnInit, Inject } from '@angular/core';
import { Client } from '../entity/client';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { AuthService } from '../repository/auth.service';

@Component({
  selector: 'app-update-profil',
  templateUrl: './update-profil.component.html',
  styleUrls: ['./update-profil.component.css']
})
export class UpdateProfilComponent implements OnInit {
   Inject
  user: Client;
  repeat: string = null;
  message:string;
 // variable des components du form angular material
  hide = true;
  hide2 = true;
  constructor( private authRepo: AuthService, public dialogRef: MatDialogRef<UpdateProfilComponent>,
     @Inject(MAT_DIALOG_DATA) public data?: any

    ) {this.user = this.data.user }

  ngOnInit() {
    console.log(this.user);
    
  }
  onNoClick(): void {
    this.dialogRef.close();
   document.location.reload(false)
  }
update(){
  console.log(this.user);
  
this.authRepo.updateUser(this.user).subscribe()
}
}
